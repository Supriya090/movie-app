import React from "react";

function SearchInput(props) {
  return (
    <div className='col col-sm-5'>
      <input
        className='form-control search-box'
        value={props.value}
        onChange={(e) => {
          props.setSearchValue(e.target.value);
        }}
        placeholder='Search for a movie'
      />
    </div>
  );
}

export default SearchInput;
