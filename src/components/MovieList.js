import React from "react";

function MovieList(props) {
  const FavouriteComponent = props.favouriteComponent;
  return (
    <>
      {props.movies.map((movie) => (
        <div className='image-container d-flex justify-content-start m-3 col'>
          <img
            src={movie.Poster}
            alt={movie.Title}
            style={{ marginBottom: "20px" }}
            className='image'
          />
          <div
            className='overlay d-flex align-items-center justify-content-center'
            onClick={() => props.handleFavouritesClick(movie)}>
            <FavouriteComponent />
          </div>
        </div>
      ))}
    </>
  );
}

export default MovieList;
